# High Level Software: Technical Test

## About
- This project was written in PHP 7.1

## How To Use
- First install the dependencies: `composer install`
- Then run the entry poin file: `php index.php`
- For unit tests run: `composer test`
- For code standards adherence run: `composer cs`

## Contact Me
If you have any questions you can send me an email at [roger.barnfather@gmail.com](mailto:rogerbarnfather@gmail.com).

