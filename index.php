<?php declare(strict_types=1);

ini_set('display_errors', 'On');
error_reporting(E_ALL);
date_default_timezone_set('Europe/London');

require_once 'vendor/autoload.php';

use App\Shop;
use App\Schedules\Parser;
use App\Schedules\Weekly\Day;

$parser = new Parser;

$holidays = require 'config/holidays.config.php';
$times = require 'config/times.config.php';

$annual = $parser->parseAnnualScheduleConfig($holidays);
$weekly = $parser->parseWeeklyScheduleConfig($times);

$shop = new Shop($annual, $weekly);

/* ----------------------- */
/* ---- Demonstration ---- */
/* ----------------------- */

echo "\n1) List opening times for any day of the week. (Example Thursday):\n\n";

echo $weekly->getDay(Day::THURSDAY);

echo "\n\n...or for all days, echo the weekly schedule:\n\n";

echo $weekly;

echo "\n\n2) List holiday dates.\n\n";

echo $annual;

echo "\n\n3) a) Query if shop is open or closed. (Example 2017-03-14 09:00 = Open, " .
    "2017-12-25 15:00 = Closed):\n\n";

$date1 = '2017-03-14 09:00';
echo $date1 . ': ';
echo $shop->isOpen(new DateTime($date1)) ? ' Open' : ' Closed';

echo "\n\n";

$date2 = '2017-12-25 15:00';
echo $date2 . ': ';
echo $shop->isOpen(new DateTime($date2)) ? ' Open' : ' Closed';

echo "\n\n";

echo "\n\n3) b) Query when shop will be closed next. (Example 2017-03-14 09:00 " .
    "= 2017-03-14 12:20):\n\n";

echo $shop->nextClosed(new DateTime('2017-03-14 09:00'))->format('Y-m-d H:i');

echo "\n\n";

echo "\n\n3) c) Query when shop will be open next.\n" .
    "    (Example, the day before Good Friday 2017-04-13 17:00 causes it\n" .
    "    to skip the whole weekend and give 2017-04-17 09:00):\n\n";

echo $shop->nextOpen(new DateTime('2017-04-13 17:00'))->format('Y-m-d H:i');

echo "\n\n";

echo "\n\n-- END --\n\n";


exit;
