<?php declare(strict_types=1);

namespace Tests\Fakes\App;

use App\Shop;
use DateTime;

class ShopFake extends Shop
{
    public $currentTimeCalled = false;

    public function currentTime() : DateTime
    {
        $this->currentTimeCalled = true;

        return parent::currentTime();
    }
}