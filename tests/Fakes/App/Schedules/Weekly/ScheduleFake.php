<?php declare(strict_types=1);

namespace Tests\Fakes\App\Schedules\Weekly;

use App\Schedules\Weekly\Day;
use App\Schedules\Weekly\Schedule;
use App\Exceptions\NoSuchWeekdayException;

class ScheduleFake extends Schedule
{
    public $getDayThrows = false;

    public function getDay(string $name) : Day
    {
        if ($this->getDayThrows) {
            throw new NoSuchWeekdayException;
        }
        
        return new Day;
    }
}
