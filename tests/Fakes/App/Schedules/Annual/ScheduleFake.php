<?php declare(strict_types=1);

namespace Tests\Fakes\App\Schedules\Annual;

use DateTime;
use OutOfRangeException;
use App\Schedules\Annual\Holiday;
use App\Schedules\Annual\Schedule;

class ScheduleFake extends Schedule
{
    public $setHolidayThrows = false;
    public $holidayCount = 0;
    public $findHolidayResult = null;
    public $isHolidayResult = false;

    public function setHoliday(string $name, Holiday $hoilday) : Schedule
    {
        if ($this->setHolidayThrows) {
            throw new OutOfRangeException;
        }

        return $this;
    }

    public function countHolidays() : int
    {
        return $this->holidayCount;
    }

    public function findHoliday(DateTime $dt) : ?Holiday
    {
        return $this->findHolidayResult;
    }

    public function isHoliday(DateTime $dt) : bool
    {
        return $this->isHolidayResult;
    }
}
