<?php declare(strict_types=1);

namespace Tests\Unit\App;

use DateTime;
use App\Shop;
use Tests\Fakes\App\ShopFake;
use PHPUnit\Framework\TestCase;
use App\Schedules\Annual\Schedule as Annual;
use App\Schedules\Weekly\Schedule as Weekly;
use Tests\Fakes\App\Schedules\Annual\ScheduleFake as FakeAnnual;
use Tests\Fakes\App\Schedules\Weekly\ScheduleFake as FakeWeekly;
use Tests\Samples\App\Schedules\Annual\ScheduleSample as SampleAnnual;
use Tests\Samples\App\Schedules\Weekly\ScheduleSample as SampleWeekly;

class ShopTest extends TestCase
{
    private $shop = null;
    private $annual = null;
    private $weekly = null;
    private $fakeAnnual = null;
    private $fakeWeekly = null;
    private $sampleAnnual = null;
    private $sampleWeekly = null;

    public function setUp()
    {
        $this->annual = new Annual;
        $this->weekly = new Weekly;
        $this->fakeAnnual = new FakeAnnual;
        $this->fakeWeekly = new FakeWeekly;
        $this->sampleAnnual = new SampleAnnual;
        $this->sampleWeekly = new SampleWeekly;

        $this->shop = new Shop($this->annual, $this->weekly);
    }

    /**
     * @test
     */
    public function constructs_with_valid_arguments()
    {
        $this->assertInstanceOf(Shop::class, $this->shop);
    }

    /**
     * @test
     */
    public function isOpen_returns_false_by_default()
    {
        $this->assertFalse($this->shop->isOpen());
    }

    /**
     * @test
     */
    public function isOpen_uses_current_datetime_if_none_given()
    {
        $mock = new ShopFake($this->annual, $this->weekly);

        $mock->isOpen();

        $this->assertTrue($mock->currentTimeCalled);
    }

    /**
     * @test
     */
    public function isOpen_returns_true_for_valid_datetimes()
    {
        $shop = new Shop($this->sampleAnnual, $this->sampleWeekly);

        // Normal day morning.
        $date1 = new DateTime('2017-02-02 11:00');
        // Normal day afternoon.
        $date2 = new DateTime('2017-02-02 13:00');
        // Day before a holiday.
        $date3 = new DateTime('2017-04-13 16:59');

        $this->assertTrue($shop->isOpen($date1));
        $this->assertTrue($shop->isOpen($date2));
        $this->assertTrue($shop->isOpen($date3));
    }

    /**
     * @test
     */
    public function isClosed_uses_current_datetime_if_none_given()
    {
        $mock = new ShopFake($this->annual, $this->weekly);
        
        $mock->isClosed();

        $this->assertTrue($mock->currentTimeCalled);
    }

    /**
     * @test
     */
    public function isClosed_returns_true_for_closed_datetimes()
    {
        $closedTimes = [
            new DateTime('2017-02-01 12:30'),
            new DateTime('2017-04-14 09:20'),
            new DateTime('2017-12-28 15:00')
        ];

        $shop = new Shop($this->sampleAnnual, $this->sampleWeekly);

        foreach ($closedTimes as $closedTime) {
            $this->assertTrue($shop->isClosed($closedTime));
        }
    }

    /**
     * @test
     */
    public function isClosed_returns_false_for_open_datetimes()
    {
        $openTimes = [
            new DateTime('2017-02-01 11:59'),
            new DateTime('2017-04-13 09:00'),
            new DateTime('2017-12-01 16:59')
        ];

        $shop = new Shop($this->sampleAnnual, $this->sampleWeekly);

        foreach ($openTimes as $openTime) {
            $this->assertFalse($shop->isClosed($openTime));
        }
    }

    /**
     * @test
     */
    public function nextClosed_returns_given_datetime_if_shop_is_already_closed()
    {
        $shop = new Shop($this->fakeAnnual, $this->fakeWeekly);

        $this->fakeAnnual->isHolidayResult = true;

        $date = new DateTime('2017-06-06 11:00');
        $dt = $shop->nextClosed($date);

        $this->assertSame($date, $dt);
    }

    /**
     * @test
     */
    public function nextClosed_uses_current_datetime_if_none_given()
    {
        $mock = new ShopFake($this->fakeAnnual, $this->fakeWeekly);

        $mock->nextClosed();

        $this->assertTrue($mock->currentTimeCalled);
    }

    /**
     * @test
     */
    public function nextClosed_works_when_both_holidays_and_closed_weekdays_are_used()
    {
        $interestingTimes = [
            // A Friday holiday should return the same datetime, regardless of time.
            [new DateTime('2017-04-14 09:00'), new DateTime('2017-04-14 09:00')],
            // An open day at lunch should return the same datetime.
            [new DateTime('2017-08-11 12:00'), new DateTime('2017-08-11 12:00')],
            // An open day in the morning should return lunch time.
            [new DateTime('2017-08-11 09:00'), new DateTime('2017-08-11 12:00')],
            // The last minute of a long holiday should return the same time.
            [new DateTime('2017-12-31 23:59'), new DateTime('2017-12-31 23:59')]
        ];

        $shop = new Shop($this->sampleAnnual, $this->sampleWeekly);

        foreach ($interestingTimes as $d) {
            $this->assertEquals($d[1], $shop->nextClosed($d[0]));
        }
    }

    /**
     * @test
     */
    public function nextOpen_uses_current_datetime_if_none_given()
    {
        $mock = new ShopFake($this->annual, $this->weekly);

        // This also tests that empty "weekly schedule"
        // doesn't cause infinite recursion.
        $mock->nextOpen();

        $this->assertTrue($mock->currentTimeCalled);
    }

    /**
     * @test
     */
    public function nextOpen_returns_given_datetime_if_shop_is_already_open()
    {
        $mock = new ShopFake($this->sampleAnnual, $this->sampleWeekly);

        $dt = new DateTime('2017-02-02 11:00');
        $result = $mock->nextOpen($dt);

        $this->assertSame($dt, $result);
    }

    /**
     * @test
     */
    public function nextOpen_returns_correct_datetime_with_typical_data()
    {
        $interestingTimes = [
            // The day before a Friday holiday at 5pm should skip to Monday 9am.
            [new DateTime('2017-04-13 17:00'), new DateTime('2017-04-17 09:00')],
            // A Friday at 5pm should skip to Monday 9am.
            [new DateTime('2017-08-11 17:00'), new DateTime('2017-08-14 09:00')],
            // A lunch break should skip to after lunch.
            [new DateTime('2017-08-11 12:45'), new DateTime('2017-08-11 13:00')],
            // Just before Christmas at 5pm should skip to the new year 9am.
            [new DateTime('2017-12-24 17:00'), new DateTime('2018-01-01 09:00')]
        ];

        $shop = new Shop($this->sampleAnnual, $this->sampleWeekly);

        foreach ($interestingTimes as $d) {
            $this->assertEquals($d[1], $shop->nextOpen($d[0]));
        }
    }
}
