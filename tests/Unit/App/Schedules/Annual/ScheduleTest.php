<?php declare(strict_types=1);

namespace Tests\Unit\App\Schedules\Annual;

use DateTime;
use PHPUnit\Framework\TestCase;
use App\Schedules\Annual\Holiday;
use App\Schedules\Annual\Schedule;

class ScheduleTest extends TestCase
{
    /**
     * @test
     */
    public function initialises_with_zero_holidays()
    {
        $this->assertEquals(0, (new Schedule)->countHolidays());
    }

    /**
     * @expectedException OutOfRangeException
     * @test
     */
    public function throws_exception_if_setting_holiday_with_empty_key()
    {
        $holiday = new Holiday(
            new DateTime('2000-01-01'),
            new DateTime('2000-01-01')
        );
        
        (new Schedule)->setHoliday('', $holiday);
    }

    /**
     * @test
     */
    public function adds_holiday_with_valid_key_and_returns_itself()
    {
        $holiday = new Holiday(new DateTime('2000-01-01'), new DateTime('2000-01-01'));
        $schedule = new Schedule;
        $reference = $schedule->setHoliday('xmas', $holiday);

        $this->assertSame($reference, $schedule);
        $this->assertEquals(1, $schedule->countHolidays());
    }

    /**
     * @test
     */
    public function findHoliday_returns_null_if_date_is_not_during_a_holiday()
    {
        $holiday1 = new Holiday(new DateTime('2000-01-02'), new DateTime('2000-01-02'));
        $holiday2 = new Holiday(new DateTime('2000-04-15'), new DateTime('2000-04-20'));
        $holiday3 = new Holiday(new DateTime('2000-12-25'), new DateTime('2000-12-26'));

        $schedule = new Schedule;
        $schedule->setHoliday('holiday1', $holiday1)
                 ->setHoliday('holiday2', $holiday2)
                 ->setHoliday('holiday3', $holiday3);
        
        $this->assertNull($schedule->findHoliday(new DateTime('2000-01-01')));
        $this->assertNull($schedule->findHoliday(new DateTime('2000-04-14')));
        $this->assertNull($schedule->findHoliday(new DateTime('2000-12-31')));
    }

    /**
     * @test
     */
    public function findHoliday_returns_holiday_for_given_date()
    {
        $holiday1 = new Holiday(new DateTime('2000-01-02'), new DateTime('2000-01-02'));
        $holiday2 = new Holiday(new DateTime('2000-04-15'), new DateTime('2000-04-20'));
        $holiday3 = new Holiday(new DateTime('2000-12-25'), new DateTime('2000-12-26'));

        $schedule = new Schedule;
        $schedule->setHoliday('holiday1', $holiday1)
                 ->setHoliday('holiday2', $holiday2)
                 ->setHoliday('holiday3', $holiday3);
        
        $date = new DateTime('2000-04-19');
        
        $this->assertSame($holiday2, $schedule->findHoliday($date));
    }

    /**
     * @test
     */
    public function isHoliday_works_as_expected()
    {
        $holiday1 = new Holiday(new DateTime('2000-01-02'), new DateTime('2000-01-04'));
        $schedule = new Schedule;
        $schedule->setHoliday('holiday1', $holiday1);

        $this->assertFalse($schedule->isHoliday(new DateTime('2000-01-01')));
        $this->assertFalse($schedule->isHoliday(new DateTime('2000-01-05')));
        $this->assertTrue($schedule->isHoliday(new DateTime('2000-01-02')));
        $this->assertTrue($schedule->isHoliday(new DateTime('2000-01-03')));
        $this->assertTrue($schedule->isHoliday(new DateTime('2000-01-04')));
    }

    /**
     * @test
     */
    public function toString_works_as_expected()
    {
        $holiday1 = new Holiday(new DateTime('2000-01-02'), new DateTime('2000-01-02'));
        $holiday2 = new Holiday(new DateTime('2000-04-15'), new DateTime('2000-04-20'));
        
        $schedule = new Schedule;
        $schedule->setHoliday('holiday1', $holiday1)
                 ->setHoliday('holiday2', $holiday2);
        
        $expected = "holiday1:\n2000-01-02 - 2000-01-02\n\nholiday2:\n2000-04-15 - 2000-04-20\n\n";

        $this->assertEquals($expected, (string)$schedule);
    }
}
