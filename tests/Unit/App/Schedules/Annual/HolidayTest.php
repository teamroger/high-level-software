<?php declare(strict_types=1);

namespace Tests\Unit\App\Schedules\Annual;

use DateTime;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use App\Schedules\Annual\Holiday;

class HolidayTest extends TestCase
{
    /**
     * @test
     */
    public function constructs_with_valid_input()
    {
        $date1 = new DateTime('2000-01-01');
        $date2 = new DateTime('2000-01-02');

        $holiday = new Holiday($date1, $date2);
        
        $this->assertSame($date1, $holiday->getStart());
        $this->assertSame($date2, $holiday->getEnd());
    }

    /**
     * @test
     * @expectedException \App\Exceptions\HolidayDateException
     */
    public function throws_when_end_date_is_less_than_start_date()
    {
        $date1 = new DateTime('2000-01-02');
        $date2 = new DateTime('2000-01-01');

        new Holiday($date1, $date2);
    }

    /**
     * @test
     */
    public function inProgress_returns_true_when_date_during_the_holiday()
    {
        $holiday = new Holiday(new DateTime('2000-01-01'), new DateTime('2000-01-02'));

        $date1 = new DateTime('2000-01-01 00:00:00.0');
        $date2 = new DateTime('2000-01-02 23:59:59.999999');
        $date3 = new DateTime('2000-01-01 12:00');

        $this->assertTrue($holiday->inProgress($date1));
        $this->assertTrue($holiday->inProgress($date2));
        $this->assertTrue($holiday->inProgress($date3));
    }

    /**
     * @test
     */
    public function inProgress_returns_false_when_date_not_during_holiday()
    {
        $holiday = new Holiday(new DateTime('2000-01-02'), new DateTime('2000-01-04'));

        $date1 = new DateTime('2000-01-01');
        $date2 = new DateTime('2000-01-05');

        $this->assertFalse($holiday->inProgress($date1));
        $this->assertFalse($holiday->inProgress($date2));
    }

    /**
     * @test
     */
    public function toString_works_as_expected()
    {
        $holiday = new Holiday(new DateTime('2000-01-02'), new DateTime('2000-01-04'));
        $expected = '2000-01-02 - 2000-01-04';
        
        $this->assertEquals($expected, (string)$holiday);
    }
}
