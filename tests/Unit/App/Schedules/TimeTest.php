<?php declare(strict_types=1);

namespace Tests\Unit\App\Schedules;

use DateTime;
use App\Schedules\Time;
use PHPUnit\Framework\TestCase;

class TimeTest extends TestCase
{
    /**
     * @test
     */
    public function constructs_with_safe_input()
    {
        $time = new Time(13, 45);

        $this->assertEquals(13, $time->getHour());
        $this->assertEquals(45, $time->getMinute());
    }

    /**
     * @test
     */
    public function constructs_with_edge_case_input()
    {
        $time1 = new Time(0, 0);
        
        $this->assertEquals(0, $time1->getHour());
        $this->assertEquals(0, $time1->getMinute());

        $time2 = new Time(23, 59);

        $this->assertEquals(23, $time2->getHour());
        $this->assertEquals(59, $time2->getMinute());
    }

    /**
     * @test
     */
    public function creates_from_date_time()
    {
        $dateTime = new DateTime('2000-03-14 09:27');
        $time = Time::fromDateTime($dateTime);

        $this->assertInstanceOf(Time::class, $time);
        $this->assertEquals(9, $time->getHour());
        $this->assertEquals(27, $time->getMinute());
    }

    /**
     * @test
     * @expectedException OutOfRangeException
     */
    public function throws_when_hour_is_below_zero()
    {
        $time = new Time(-1, 0);
    }

    /**
     * @test
     * @expectedException OutOfRangeException
     */
    public function throws_when_hour_is_above_twenty_three()
    {
        $time = new Time(24, 0);
    }

    /**
     * @test
     * @expectedException OutOfRangeException
     */
    public function throws_when_minute_is_below_zero()
    {
        $time = new Time(0, -1);
    }

    /**
     * @test
     * @expectedException OutOfRangeException
     */
    public function throws_when_minute_is_above_fifty_nine()
    {
        $time = new Time(0, 60);
    }

    /**
     * @test
     */
    public function gt_returns_true_with_same_hour_part_different_minute_part()
    {
        $time1 = new Time(9, 31);
        $time2 = new Time(9, 30);

        $isGreater = $time1->gt($time2);

        $this->assertTrue($isGreater);
    }

    /**
     * @test
     */
    public function gt_returns_true_with_different_hour_part_same_minute_part()
    {
        $time1 = new Time(10, 30);
        $time2 = new Time(9, 30);

        $isGreater = $time1->gt($time2);

        $this->assertTrue($isGreater);
    }

    /**
     * @test
     */
    public function gt_returns_false_with_same_hour_part_different_minute_part()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 31);

        $isGreater = $time1->gt($time2);

        $this->assertFalse($isGreater);
    }

    /**
     * @test
     */
    public function gt_returns_false_with_different_hour_part_same_minute_part()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(10, 30);

        $isGreater = $time1->gt($time2);

        $this->assertFalse($isGreater);
    }

    /**
     * @test
     */
    public function gt_returns_false_with_equal_times()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 30);

        $isGreater = $time1->gt($time2);

        $this->assertFalse($isGreater);
    }

    /**
     * @test
     */
    public function lt_returns_true_with_same_hour_part_different_minute_part()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 31);

        $isLess = $time1->lt($time2);

        $this->assertTrue($isLess);
    }

    /**
     * @test
     */
    public function lt_returns_true_with_different_hour_part_same_minute_part()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(10, 30);

        $isLess = $time1->lt($time2);

        $this->assertTrue($isLess);
    }

    /**
     * @test
     */
    public function lt_returns_false_with_same_hour_part_different_minute_part()
    {
        $time1 = new Time(9, 31);
        $time2 = new Time(9, 30);

        $isLess = $time1->lt($time2);

        $this->assertFalse($isLess);
    }

    /**
     * @test
     */
    public function lt_returns_false_with_different_hour_part_same_minute_part()
    {
        $time1 = new Time(10, 30);
        $time2 = new Time(9, 30);

        $isLess = $time1->lt($time2);

        $this->assertFalse($isLess);
    }

    /**
     * @test
     */
    public function lt_returns_false_with_equal_times()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 30);

        $isLess = $time1->lt($time2);

        $this->assertFalse($isLess);
    }

    /**
     * @test
     */
    public function eq_returns_false_with_minute1_greater_than_minute2()
    {
        $time1 = new Time(9, 31);
        $time2 = new Time(9, 30);

        $isEqual = $time1->eq($time2);

        $this->assertFalse($isEqual);
    }

    /**
     * @test
     */
    public function eq_returns_false_with_minute1_less_than_minute2()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 31);

        $isEqual = $time1->eq($time2);

        $this->assertFalse($isEqual);
    }

    /**
     * @test
     */
    public function eq_returns_false_with_hour1_greater_than_hour2()
    {
        $time1 = new Time(10, 30);
        $time2 = new Time(9, 30);

        $isEqual = $time1->eq($time2);

        $this->assertFalse($isEqual);
    }

    /**
     * @test
     */
    public function eq_returns_false_with_hour1_less_than_hour2()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(10, 30);

        $isEqual = $time1->eq($time2);

        $this->assertFalse($isEqual);
    }

    /**
     * @test
     */
    public function eq_returns_true_with_equal_times()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 30);
        
        $isEqual = $time1->eq($time2);

        $this->assertTrue($isEqual);
    }

    /**
     * @test
     */
    public function gte_returns_true_with_equal_or_lower_time_as_parameter()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 30);

        $this->assertTrue($time1->gte($time2));

        $time1 = new Time(9, 31);
        $time2 = new Time(9, 30);

        $this->assertTrue($time1->gte($time2));
    }

    /**
     * @test
     */
    public function gte_returns_false_with_greater_time_as_parameter()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 31);

        $this->assertFalse($time1->gte($time2));
    }

    /**
     * @test
     */
    public function lte_returns_true_with_equal_or_higher_time_as_parameter()
    {
        $time1 = new Time(9, 30);
        $time2 = new Time(9, 30);

        $this->assertTrue($time1->lte($time2));

        $time1 = new Time(9, 30);
        $time2 = new Time(9, 31);

        $this->assertTrue($time1->lte($time2));
    }

    /**
     * @test
     */
    public function lte_returns_false_with_less_time_as_parameter()
    {
        $time1 = new Time(9, 31);
        $time2 = new Time(9, 30);

        $this->assertFalse($time1->lte($time2));
    }

    /**
     * @test
     */
    public function toString_gives_expected_result()
    {
        $this->assertEquals('08:03', (string)new Time(8, 3));
        $this->assertEquals('23:59', (string)new Time(23, 59));
    }
}
