<?php declare(strict_types=1);

namespace Tests\Unit\App\Schedules\Weekly;

use DateTime;
use App\Schedules\Time;
use App\Schedules\Weekly\Day;
use App\Schedules\Weekly\Shift;
use PHPUnit\Framework\TestCase;

class DayTest extends TestCase
{
    private $day = null;
    private $shifts = [];

    public function setUp()
    {
        $this->day = new Day;
        $this->shifts = [
            new Shift(new Time(13, 0), new Time(14, 0)),
            new Shift(new Time(14, 0), new Time(15, 0)),
            new Shift(new Time(15, 0), new Time(16, 0)),
        ];
    }

    /**
     * @test
     */
    public function day_can_add_a_shift_and_return_itself()
    {
        $reference = $this->day->addShift($this->shifts[0]);

        $this->assertSame($reference, $this->day);
        $this->assertEquals(1, $this->day->countShifts());
    }

    /**
     * @test
     * @expectedException \App\Exceptions\ShiftOverlapException
     */
    public function throws_exception_when_adding_a_shift_that_overlaps()
    {
        $shift1 = new Shift(new Time(13, 0), new Time(14, 0));
        $shift2 = new Shift(new Time(13, 30), new Time(14, 30));
        
        $this->day->addShift($shift1)->addShift($shift2);
    }

    /**
     * @test
     */
    public function can_add_shifts_that_do_not_overlap()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);

        $this->assertEquals(3, $this->day->countShifts());
    }

    /**
     * @test
     */
    public function findShift_returns_null_when_time_does_not_match_a_shift()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);

        $this->assertNull($this->day->findShift(new Time(12, 59)));
    }

    /**
     * @test
     */
    public function findShift_returns_correct_shift_when_time_matches_a_shift()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);
        
        $shift = $this->day->findShift(new Time(14, 30));

        $this->assertSame($shift, $this->shifts[1]);
    }

    /**
     * @test
     */
    public function is_not_open_by_default()
    {
        $this->assertFalse($this->day->isOpen(new Time(14, 0)));
    }

    /**
     * @test
     */
    public function is_open_when_time_matches_a_shift()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);
        
        $this->assertTrue($this->day->isOpen(new Time(15, 59)));
    }

    /**
     * @test
     */
    public function is_not_open_when_time_does_not_match_a_shift()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);
        
        $this->assertFalse($this->day->isOpen(new Time(7, 45)));
    }

    /**
     * @test
     */
    public function is_closed_all_day_by_default()
    {
        $this->assertTrue($this->day->closedAllDay());
    }

    /**
     * @test
     */
    public function not_closed_all_day_after_adding_a_shift()
    {
        $this->day->addShift($this->shifts[0]);
        
        $this->assertFalse($this->day->closedAllDay());
    }

    /**
     * @test
     */
    public function nextShift_returns_null_when_time_is_later_than_all_shifts()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);
        
        $this->assertNull($this->day->nextShift(new Time(19, 0)));
    }
    
    /**
     * @test
     */
    public function nextShift_returns_first_shift_when_time_is_before_it()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);
        
        $this->assertSame(
            $this->shifts[0],
            $this->day->nextShift(new Time(10, 0))
        );
    }

    /**
     * @test
     */
    public function nextShift_returns_same_shift_if_time_intersects_it()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);
        
        $this->assertSame(
            $this->shifts[1],
            $this->day->nextShift($this->shifts[1]->getStart())
        );
    }

    /**
     * @test
     */
    public function nextShift_returns_the_next_shift_for_time_between_shifts()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[2]);

        $shift = $this->day->nextShift(new Time(14, 30));

        $this->assertSame($this->shifts[2], $shift);
    }

    /**
     * @test
     */
    public function toString_works_as_expected()
    {
        $this->day
            ->addShift($this->shifts[0])
            ->addShift($this->shifts[1])
            ->addShift($this->shifts[2]);
        
        $expected = "13:00 - 14:00\n14:00 - 15:00\n15:00 - 16:00";

        $this->assertEquals($expected, (string)$this->day);
    }
}
