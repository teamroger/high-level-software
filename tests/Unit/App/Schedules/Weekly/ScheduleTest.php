<?php declare(strict_types=1);

namespace Tests\Unit\App\Schedules\Weekly;

use App\Schedules\Weekly\Day;
use PHPUnit\Framework\TestCase;
use App\Schedules\Weekly\Schedule;

class ScheduleTest extends TestCase
{
    private $schedule = null;

    public function setUp()
    {
        $this->schedule = new Schedule;
    }

    /**
     * @test
     */
    public function constructs_and_contains_all_weekdays()
    {
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::MONDAY));
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::TUESDAY));
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::WEDNESDAY));
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::THURSDAY));
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::FRIDAY));
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::SATURDAY));
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::SUNDAY));
    }

    /**
     * @test
     */
    public function is_empty_until_a_day_has_a_shift()
    {
        $this->assertTrue($this->schedule->empty());

        $shift = new \App\Schedules\Weekly\Shift(
            new \App\Schedules\Time(14, 0),
            new \App\Schedules\Time(15, 0)
        );
        $this->schedule->getDay(Day::FRIDAY)->addShift($shift);

        $this->assertFalse($this->schedule->empty());
    }

    /**
     * @test
     */
    public function can_get_a_day_by_correct_name()
    {
        $this->assertInstanceOf(Day::class, $this->schedule->getDay(Day::SUNDAY));
    }

    /**
     * @expectedException \App\Exceptions\NoSuchWeekdayException
     * @test
     */
    public function throws_exception_getting_day_by_invalid_name()
    {
        $this->schedule->getDay('Not a day name');
    }

    /**
     * @test
     */
    public function toString_works_as_expected()
    {
        $days = [
            Day::MONDAY, Day::TUESDAY, Day::WEDNESDAY,
            Day::THURSDAY, Day::FRIDAY, Day::SATURDAY, Day::SUNDAY
        ];

        $expected = implode(":\n(closed)\n\n", $days) . ":\n(closed)\n\n";
        
        $this->assertEquals($expected, (string)$this->schedule);
    }
}
