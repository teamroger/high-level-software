<?php declare(strict_types=1);

namespace Tests\Unit\App\Schedules\Weekly;

use App\Schedules\Time;
use App\Schedules\Weekly\Shift;
use PHPUnit\Framework\TestCase;

class ShiftTest extends TestCase
{
    private $shift = null;

    public function setUp()
    {
        $this->shift = new Shift(new Time(13, 0), new Time(15, 59));
    }

    /**
     * @test
     */
    public function constructs_with_valid_times_as_arguments()
    {
        $this->assertInstanceOf(Shift::class, $this->shift);
        $this->assertEquals(13, $this->shift->getStart()->getHour());
        $this->assertEquals(0, $this->shift->getStart()->getMinute());
        $this->assertEquals(15, $this->shift->getEnd()->getHour());
        $this->assertEquals(59, $this->shift->getEnd()->getMinute());
    }

    /**
     * @param array $startValues
     * @param array $endValues
     * @param boolean $willOverlap
     * @testWith [[11, 0], [13, 0], false]
     *           [[17, 0], [19, 0], false]
     *           [[11, 0], [13, 1], true]
     *           [[15, 58], [18, 0], true]
     *           [[13, 30], [14, 40], true]
     *           [[0, 0], [23, 59], true]
     * @test
     */
    public function can_detect_if_it_overlaps_another_shift(
        array $startValues, array $endValues, bool $willOverlap
    ) {
        $other = new Shift(
            new Time($startValues[0], $startValues[1]),
            new Time($endValues[0], $endValues[1])
        );

        $this->assertEquals($willOverlap, $this->shift->overlaps($other));
    }

    /**
     * @param int $hour
     * @param int $minute
     * @param boolean $isActive
     * @testWith [13, 0, true]
     *           [15, 58, true]
     *           [12, 59, false]
     *           [15, 59, false]
     * @test
     */
    public function is_active_when_expected(int $hour, int $minute, bool $isActive)
    {
        $time = new Time($hour, $minute);
        $this->assertEquals($isActive, $this->shift->isActive($time));
    }

    /**
     * @test
     */
    public function toString_produces_expected_string()
    {
        $this->assertEquals('13:00 - 15:59', (string)$this->shift);
    }
}
