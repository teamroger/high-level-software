<?php declare(strict_types=1);

namespace Tests\Samples\App\Schedules\Weekly;

use App\Schedules\Time;
use App\Schedules\Weekly\Day;
use App\Schedules\Weekly\Shift;

class DaySample extends Day
{
    public function __construct()
    {
        $this->addShift(
            new Shift(
                new Time(9, 0),
                new Time(12, 0)
            )
        );

        $this->addShift(
            new Shift(
                new Time(13, 0),
                new Time(17, 0)
            )
        );
    }
}
