<?php declare(strict_types=1);

namespace Tests\Samples\App\Schedules\Weekly;

use App\Schedules\Weekly\Day;
use App\Schedules\Weekly\Schedule;
use Tests\Samples\App\Schedules\Weekly\DaySample;

class ScheduleSample extends Schedule
{
    public function __construct()
    {
        parent::__construct();

        $this->days[Day::MONDAY] = new DaySample;
        $this->days[Day::TUESDAY] = new DaySample;
        $this->days[Day::WEDNESDAY] = new DaySample;
        $this->days[Day::THURSDAY] = new DaySample;
        $this->days[Day::FRIDAY] = new DaySample;
        $this->days[Day::SATURDAY] = new Day;
        $this->days[Day::SUNDAY] = new Day;
    }
}
