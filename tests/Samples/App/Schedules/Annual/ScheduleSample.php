<?php declare(strict_types=1);

namespace Tests\Samples\App\Schedules\Annual;

use DateTime;
use App\Schedules\Annual\Holiday;
use App\Schedules\Annual\Schedule;

class ScheduleSample extends Schedule
{
    public function __construct()
    {
        $holiday1 = new Holiday(new DateTime('2017-04-14'), new DateTime('2017-04-14'));
        $holiday2 = new Holiday(new DateTime('2017-05-01'), new DateTime('2017-05-01'));
        $holiday3 = new Holiday(new DateTime('2017-12-25'), new DateTime('2017-12-31'));

        $this
            ->setHoliday('holiday1', $holiday1)
            ->setHoliday('holiday2', $holiday2)
            ->setHoliday('holiday3', $holiday3);
    }
}
