<?php declare(strict_types=1);

namespace App\Schedules\Annual;

use DateTime;
use App\Exceptions\HolidayDateException;

class Holiday
{
    /**
     * Constructs a new holiday period.
     * NOTE: The start date will have its time set to 00:00:00 of that day.
     * The end date will have its time set to 23:59:59 of that day.
     * @param DateTime $start The start date.
     * @param DateTime $end The end date.
     * @throws HolidayDateException If the end date is earlier than the start.
     */
    public function __construct(DateTime $start, DateTime $end)
    {
        if ($end < $start) {
            throw new HolidayDateException('Holiday cannot end before it starts');
        }

        $this->start = $start->setTime(0, 0, 0);
        $this->end = $end->setTime(23, 59, 59, 999999);
    }

    /**
     * Gets the start date of the holiday.
     * @return DateTime The start date.
     */
    public function getStart() : DateTime
    {
        return $this->start;
    }

    /**
     * Gets the end date of the holiday.
     * @return DateTime The end date.
     */
    public function getEnd() : DateTime
    {
        return $this->end;
    }

    /**
     * Determines whether or not this holiday is under way
     * based on the given date.
     * @param DateTime $dt The date to check against.
     * @return boolean  True if the date is within this holiday,
     *                  otherwise false.
     */
    public function inProgress(DateTime $dt) : bool
    {
        $afterStart = $dt >= $this->start;
        $beforeEnd = $dt <= $this->end;
        $holidayInProgress = $afterStart && $beforeEnd;

        return $holidayInProgress;
    }

    /**
     * Gets a string representation of this holiday period.
     * The string consists of the start date, a hyphen and
     * the end date.
     * @return string The string representation of this holiday.
     */
    public function __toString() : string
    {
        $format = 'Y-m-d';

        return $this->start->format($format) .
            ' - ' .
            $this->end->format($format);
    }
}
