<?php declare(strict_types=1);

namespace App\Schedules\Annual;

use DateTime;
use OutOfRangeException;
use App\Schedules\Annual\Holiday;

/**
 * Class to represent a yearly schedule of all
 * holiday periods for which a shop would be closed.
 * @package App\Schedules\Annual
 */
class Schedule
{
    /**
     * The holiday periods.
     * @var array
     */
    private $holidays = [];

    /**
     * Sets a holiday period to this schedule.
     * @param string    $name A name for the holiday period e.g.- Christmas.
     * @param Holiday   $holiday The holiday to add.
     * @return Schedule This object for method chaining.
     * @throws OutOfRangeException If the name is empty.
     */
    public function setHoliday(string $name, Holiday $holiday) : Schedule
    {
        if ($name === '') {
            throw new OutOfRangeException(
                'Cannot use empty string as holiday name'
            );
        }

        $this->holidays[$name] = $holiday;

        return $this;
    }

    /**
     * Gets the number of holidays registered in the schedule.
     * @return int The number of holiday periods.
     */
    public function countHolidays() : int
    {
        return count($this->holidays);
    }

    /**
     * Finds a holiday occurring at the given datetime.
     * If there is not match then null is returned.
     * @param DateTime $dt The datetime to check against.
     * @return Holiday|null A holiday on that date, otherwise null.
     */
    public function findHoliday(DateTime $dt) : ?Holiday
    {
        foreach ($this->holidays as $holiday) {
            if ($holiday->inProgress($dt)) {
                return $holiday;
            }
        }

        return null;
    }

    /**
     * Determines if the given datetime happens during a holiday.
     * @param DateTime $dt The time to check against.
     * @return boolean True if the shop will be open, otherwise false.
     */
    public function isHoliday(DateTime $dt) : bool
    {
        return ($this->findHoliday($dt) !== null);
    }

    /**
     * Gets a string representation of all the holiday
     * periods in this annual schedule.
     * @return string The string representation of this schedule.
     */
    public function __toString() : string
    {
        $string = '';

        foreach ($this->holidays as $name => $holiday) {
            $string .= $name . ":\n" . (string)$holiday . "\n\n";
        }

        return $string;
    }
}
