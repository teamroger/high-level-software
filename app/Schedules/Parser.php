<?php declare(strict_types=1);

namespace App\Schedules;

use DateTime;
use App\Schedules\Time;
use App\Schedules\Weekly\Shift;
use App\Schedules\Annual\Holiday;
use App\Exceptions\ParseException;
use App\Schedules\Annual\Schedule as Annual;
use App\Schedules\Weekly\Schedule as Weekly;

/**
 * Class for parsing the basic config and producing
 * the respective schedule objects.
 * @package App\Schedules
 */
class Parser
{
    /**
     * Parses a config file of holiday data to create a yearly shchedule.
     * @param array $config The config file of holiday data.
     * @return Annual A schedule of holidays.
     */
    public function parseAnnualScheduleConfig(array $config) : Annual
    {
        $schedule = new Annual;
        $format = 'd-m-Y';

        foreach ($config as $holidayData) {
            $holiday = new Holiday(
                DateTime::createFromFormat($format, $holidayData['start']),
                DateTime::createFromFormat($format, $holidayData['end'])
            );

            $schedule->setHoliday($holidayData['description'], $holiday);
        }

        return $schedule;
    }

    /**
     * Parses a config file of weekday opening times and produces
     * a schedule of days and opening times.
     * @param array $config The config file of opening times data.
     * @return Weekly The schedule of weekdays and opening times.
     * @throws ParseException   If any of the open times aren't followed
     *                          by a corresponding closing time.
     */
    public function parseWeeklyScheduleConfig(array $config) : Weekly
    {
        $schedule = new Weekly;

        foreach ($config as $dayName => $dayData) {
            if (count($dayData) % 2) {
                $message =  'Odd number of opening/closing times.';
                throw new ParseException($message);
            }
            
            $shifts = $this->dayDataToShifts($dayData);

            foreach ($shifts as $shift) {
                $schedule->getDay($dayName)->addShift($shift);
            }
        }
        
        return $schedule;
    }

    /**
     * Converts a block of day data to an array of shifts it contains.
     * @param array $dayData The block of day data (a list of open/close times).
     * @return array An array of shift objects for the day.
     */
    private function dayDataToShifts(array $dayData) : array
    {
        $allTimes = array_keys($dayData);
        $shifts = [];

        while (!empty($allTimes)) {
            $start = Time::fromDateTime(new DateTime(array_shift($allTimes)));
            $end = Time::fromDateTime(new DateTime(array_shift($allTimes)));

            $shifts[] = new Shift($start, $end);
        }

        return $shifts;
    }
}
