<?php declare(strict_types=1);

namespace App\Schedules\Weekly;

use App\Schedules\Weekly\Day;
use App\Exceptions\NoSuchWeekdayException;

/**
 * Class to represent the opening times per week.
 * Instances of this class contain each week day from
 * Monday to Sunday on instanciation which can then
 * have opening shifts added to them.
 * @package App\Schedules\Weekly
 */
class Schedule
{
    /**
     * The days of the week for a weekly schedule.
     * @var array
     */
    protected $days = [];

    /**
     * Constructs a new weekly schedule complete
     * with day objects.
     */
    public function __construct()
    {
        $this->days[Day::MONDAY]    = new Day;
        $this->days[Day::TUESDAY]   = new Day;
        $this->days[Day::WEDNESDAY] = new Day;
        $this->days[Day::THURSDAY]  = new Day;
        $this->days[Day::FRIDAY]    = new Day;
        $this->days[Day::SATURDAY]  = new Day;
        $this->days[Day::SUNDAY]    = new Day;
    }

    /**
     * Determines if there are any shifts during the days.
     * @reurn boolean True if no day has a shift, otherwise false.
     */
    public function empty() : bool
    {
        foreach ($this->days as $day) {
            if (!$day->closedAllDay()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets a day.
     * @param string $name  The name of the day to get. It is recommended
     *                      to use a constant of the Day class
     *                      e.g.- Day::MONDAY.
     * @return Day The requested day object.
     * @throws NoSuchWeekdayException If an incorrect day name is given.
     */
    public function getDay(string $name) : Day
    {
        if (array_key_exists($name, $this->days)) {
            return $this->days[$name];
        }

        throw new NoSuchWeekdayException('Invalid day of week name "' . $name . '".');
    }

    /**
     * Gets a string representation of this weekly schedule.
     * The string includes each day of the week and their
     * open shifts.
     * @return string The string representation of this weekly schedule.
     */
    public function __toString() : string
    {
        $string = '';

        foreach ($this->days as $key => $day) {
            $string .= $key . ":\n" . (string)$day . "\n\n";
        }

        return $string;
    }
}
