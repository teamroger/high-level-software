<?php declare(strict_types=1);

namespace App\Schedules\Weekly;

use App\Schedules\Time;

/**
 * Class to represent a period of time that a shop
 * is open for. There may be many shifts in one day
 * due to lunch breaks etc.
 * @package App\Schedules\Weekly
 */
class Shift
{
    /**
     * The start time of the shift.
     * @var Time
     */
    private $start = null;

    /**
     * The end time of the shift.
     * @var Time
     */
    private $end = null;

    /**
     * Constructs a new shift.
     * @param Time $start   The start time of the shift.
     * @param Time $end     The end time of the shift.
     */
    public function __construct(Time $start, Time $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * Gets the start time of the shift.
     * @return Time The start time.
     */
    public function getStart() : Time
    {
        return $this->start;
    }

    /**
     * Gets the end time of the shift.
     * @return Time The end time.
     */
    public function getEnd() : Time
    {
        return $this->end;
    }

    /**
     * Determines whether or not the times of the
     * given shift overlap with the times of this shift.
     * e.g.- 08:00 to 12:00 would overlap with 11:00 to 13:00.
     * @param Shift $shift  The shift to check against this.
     * @return boolean      True if the shift overlaps with this,
     *                      otherwise false.
     */
    public function overlaps(Shift $shift) : bool
    {
        $shiftStart = $shift->getStart();
        $shiftEnd = $shift->getEnd();

        return ($shiftStart->lt($this->end) &&
            $shiftEnd->gt($this->start)
        );
    }

    /**
     * Determines if this shift would be active at the given time.
     * @param Time $time The time to check against.
     * @return boolean  True if the time coincides with this shift,
     *                  otherwise false.
     */
    public function isActive(Time $time) : bool
    {
        return ($time->gte($this->start) && $time->lt($this->end));
    }

    /**
     * Gets a string representation of this shift.
     * @return string   This shift's start and end times as a string
     *                  separated by a hyphen.
     */
    public function __toString() : string
    {
        return (string)$this->start . ' - ' . (string)$this->end;
    }
}
