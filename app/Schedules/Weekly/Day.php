<?php declare(strict_types=1);

namespace App\Schedules\Weekly;

use App\Schedules\Time;
use App\Exceptions\ShiftOverlapException;

/**
 * Class to represent a day that a shop may or may not
 * be open for. This class essentially exists to
 * contain a number of shifts that the shop will be
 * open for during the day.
 * @package App\Schedules\Weekly
 */
class Day
{
    const MONDAY    = 'Monday';
    const TUESDAY   = 'Tuesday';
    const WEDNESDAY = 'Wednesday';
    const THURSDAY  = 'Thursday';
    const FRIDAY    = 'Friday';
    const SATURDAY  = 'Saturday';
    const SUNDAY    = 'Sunday';

    /**
     * The shifts for this day.
     * @var array
     */
    private $shifts = [];

    /**
     * Adds a shift to this day.
     * @param   Shift $shift A shift to add to this day.
     * @return  Day This object, for method chaining.
     * @throws  ShiftOverlapException If the shift overlaps with another.
     */
    public function addShift(Shift $shift) : Day
    {
        foreach ($this->shifts as $s) {
            if ($shift->overlaps($s)) {
                $message = (string)$shift . ' vs ' . (string)$s;
                throw new ShiftOverlapException($message);
            }
        }

        $this->shifts[] = $shift;

        return $this;
    }

    /**
     * Gets the number of shifts for this day.
     * @return int The number of shifts.
     */
    public function countShifts() : int
    {
        return count($this->shifts);
    }

    /**
     * Gets the shift that is active at the given time.
     * If no shift is active then null is returned.
     * @param Time $time The time to check against.
     * @return Shift|null The shift if one matches, otherwise null.
     */
    public function findShift(Time $time) : ?Shift
    {
        foreach ($this->shifts as $shift) {
            if ($shift->isActive($time)) {
                return $shift;
            }
        }

        return null;
    }

    /**
     * Determines if the shop is open at the given time.
     * @param Time $time The current time.
     * @return boolean  True if the time coincides with an existing shift,
     *                  otherwise false.
     */
    public function isOpen(Time $time) : bool
    {
        return ($this->findShift($time) !== null);
    }

    /**
     * Determines if the day has no shifts and is therefore
     * closed all day.
     * @return boolean True if there are no shifts, otherwise false;
     */
    public function closedAllDay() : bool
    {
        return count($this->shifts) === 0;
    }

    /**
     * Gets the next shift after the give time.
     * If the given time falls within a shift then that
     * shift is returned.
     * @param Time $time The time to check against.
     * @return Shift|null The current/next shift if there is one,
     *                    otherwise null.
     */
    public function nextShift(Time $time) : ?Shift
    {
        // Assuming shifts are ordered by time,
        // the first one we hit after the given time
        // will be the next shift.
        foreach ($this->shifts as $shift) {
            if ($shift->isActive($time)) {
                return $shift;
            }

            if ($shift->getStart()->gt($time)) {
                return $shift;
            }
        }
        
        return null;
    }

    /**
     * Gets a string representation of this day.
     * The string lists all shift times or "(closed)"
     * if there are no shifts.
     * @return string A string representation of this day.
     */
    public function __toString() : string
    {
        if (empty($this->shifts)) {
            return '(closed)';
        }

        return implode("\n", $this->shifts);
    }
}
