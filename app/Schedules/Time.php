<?php declare(strict_types=1);

namespace App\Schedules;

use DateTime;
use OutOfRangeException;

/**
 * Class to represent a simplified time of the day.
 * It ignores any concept of date and only accounts for
 * hours and minutes, disregarding seconds.
 * @package App\Schedules
 */
class Time
{
    /**
     * The hour part of the time.
     * @var int
     */
    private $hour = 0;

    /**
     * The minute part of the time.
     * @var int
     */
    private $minute = 0;

    /**
     * Creates a new time object from a datetime.
     * @param DateTime $datetime The datetime object.
     * @return Time A new time object.
     */
    public static function fromDateTime(DateTime $dateTime) : Time
    {
        $hour = (int)$dateTime->format('G');
        $minute = (int)$dateTime->format('i');

        return new Time($hour, $minute);
    }

    /**
     * Constructs a new time object.
     * @param int $hour The hour part of the time.
     * @param int $minute The minute part of the time.
     * @throws OutOfRangeException If the hour or minute is invalid.
     */
    public function __construct(int $hour, int $minute)
    {
        if ($hour < 0 || $hour > 23) {
            throw new OutOfRangeException('Hour must be between 0 and 23');
        }

        if ($minute < 0 || $minute > 59) {
            throw new OutOfRangeException('Minute must be between 0 and 59');
        }

        $this->hour = $hour;
        $this->minute = $minute;
    }

    /**
     * Gets the hour part of the time.
     * @return int The hour value.
     */
    public function getHour() : int
    {
        return $this->hour;
    }

    /**
     * Gets the minute part of the time.
     * @return int The minute value.
     */
    public function getMinute() : int
    {
        return $this->minute;
    }

    /**
     * Determines if this time is equal to the given time.
     * @param Time $time The time to compare this to.
     * @return boolean True if both times are equal, otherwise false.
     */
    public function eq(Time $time) : bool
    {
        return $this->hour === $time->getHour() &&
            $this->minute === $time->getMinute();
    }

    /**
     * Determines if this time is greater than the given time.
     * @param Time $time the time to compare this to.
     * @return boolean True if this time is greater, otherwise false.
     */
    public function gt(Time $time) : bool
    {
        if ($this->hour < $time->getHour()) {
            return false;
        }

        if ($this->hour === $time->getHour()) {
            return $this->minute > $time->getMinute();
        }

        return true;
    }

    /**
     * Determines if this time is greater than or equal to the given time.
     * @param Time $time The time to compare.
     * @return boolean True if this time is greater or equal, otherwise false.
     */
    public function gte(Time $time) : bool
    {
        return ($this->gt($time) || $this->eq($time));
    }

    /**
     * Determines if this time is greater than the given time.
     * @param Time $time The time to compare this to.
     * @return boolean True if this time is less, otherwise false.
     */
    public function lt(Time $time) : bool
    {
        return !$this->gte($time);
    }

    /**
     * Determines if this time is less than or equal to the given time.
     * @param Time $time The time to compare.
     * @return boolean True if this time is less or equal, otherwise false.
     */
    public function lte(Time $time) : bool
    {
        return ($this->lt($time) || $this->eq($time));
    }

    /**
     * Gets the string representation of this time.
     * @return string The string representation of this time in 24hr notation.
     */
    public function __toString() : string
    {
        return  sprintf('%02d:%02d', $this->hour, $this->minute);
    }
}
