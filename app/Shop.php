<?php declare(strict_types=1);

namespace App;

use DateTime;
use DateInterval;
use App\Schedules\Time;
use App\Schedules\Annual\Schedule as Annual;
use App\Schedules\Weekly\Schedule as Weekly;

/**
 * Class to represent a shop and its scheduled opening hours and holidays.
 * @package App
 */
class Shop
{
    /**
     * The annual schedule containing holiday periods.
     * @var Annual
     */
    private $annualSchedule = null;

    /**
     * The weekly schedule of days and their opening hours.
     * @var Weekly
     */
    private $weeklySchedule = null;
    
    /**
     * Constructs a new shop object.
     * @param Annual $as An annual schedule of holiday periods.
     * @param Weekly $ws A weekly schedule of days and opening hours.
     */
    public function __construct(Annual $as, Weekly $ws)
    {
        $this->annualSchedule = $as;
        $this->weeklySchedule = $ws;
    }

    /**
     * Is the shop open on the provided date/time
     * If provided a DateTime object, check relative to that, otherwise use now
     *
     * @param DateTime $dt
     * @return boolean
     */
    public function isOpen(DateTime $dt = null)
    {
        if ($dt === null) {
            $dt = $this->currentTime();
        }
        
        if ($this->annualSchedule->isHoliday($dt)) {
            return false;
        }
    
        $dayName = $dt->format('l');
        $day = $this->weeklySchedule->getDay($dayName);
        $isOpen = $day->isOpen(Time::fromDateTime($dt));

        return $isOpen;
    }


    /**
     * Is the shop closed on the provided date/time
     * If provided a DateTime object, check relative to that, otherwise use now
     *
     * @param DateTime $dt
     * @return boolean
     */
    public function isClosed(DateTime $dt = null)
    {
        return !$this->isOpen($dt);
    }


    /**
     * At what date/time will the shop next be open
     * If provided a DateTime object, check relative to that, otherwise use now
     * If the shop is already open, return the provided datetime/now
     *
     * @param DateTime $dt
     * @return DateTime
     */
    public function nextOpen(DateTime $dt = null)
    {
        if ($dt === null) {
            $dt = $this->currentTime();
        }

        if ($this->isOpen($dt)) {
            return $dt;
        }

        // Prevent infinite recursion for totally empty week.
        if ($this->weeklySchedule->empty()) {
            return $dt;
        }

        $holiday = $this->annualSchedule->findHoliday($dt);

        if ($holiday !== null) {
            $end = clone $holiday->getEnd();
            
            // Go to the day after the holiday ends.
            $end->add(new DateInterval('P1D'));
            $end->setTime(0, 0, 0, 0);

            return $this->nextOpen($end);
        }

        $day = $this->weeklySchedule->getDay($dt->format('l'));

        if ($day->closedAllDay()) {
            $nextDay = clone $dt;
            $nextDay->add(new DateInterval('P1D'));
            $nextDay->setTime(0, 0, 0, 0);

            return $this->nextOpen($nextDay);
        }

        $shift = $day->nextShift(Time::fromDateTime($dt));

        if ($shift === null) {
            $nextMorning = clone $dt;
            $nextMorning->add(new DateInterval('P1D'));
            $nextMorning->setTime(0, 0, 0, 0);

            return $this->nextOpen($nextMorning);
        }

        $startTime = $shift->getStart();

        $start = clone $dt;
        $start->setTime($startTime->getHour(), $startTime->getMinute());

        return $start;
    }


    /**
     * At what date/time will the shop next be closed
     * If provided a DateTime object, check relative to that, otherwise use now
     * If the shop is already closed, return the provided datetime/now
     *
     * @param DateTime $dt
     * @return DateTime
     */
    public function nextClosed(DateTime $dt = null)
    {
        if ($dt === null) {
            $dt = $this->currentTime();
        }

        if ($this->isOpen($dt)) {
            $time        = Time::fromDateTime($dt);
            $dayName     = $dt->format('l');
            $day         = $this->weeklySchedule->getDay($dayName);
            $shift       = $day->findShift($time);
            $closingTime = $shift->getEnd();

            // Don't modify the original datetime.
            $t = clone $dt;
            $t->setTime($closingTime->getHour(), $closingTime->getMinute());
            return $t;
        }

        return $dt;
    }

    /**
     * Gets the current datetime.
     * @return DateTime The current datetime.
     */
    protected function currentTime() : DateTime
    {
        return new DateTime;
    }
}
